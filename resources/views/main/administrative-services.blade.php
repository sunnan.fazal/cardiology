<section class="inner-header divider parallax layer-overlay overlay-white-4" data-bg-img="{{ url('/resources/images/bg/heart.jpg') }}" style="background-image: url({{ url('/resources/images/bg/heart.jpg') }});background-position: 50% -200px !important;height: 400px">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title" style="color: white">Services</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

 <section id="depertment" class="bg-silver-light bg-lighter">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase mt-0 line-height-1">Administrative Services</h2>
              <div class="title-icon">
                <img class="mb-10" src="images/title-icon.png" alt="">
              </div>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/1.jpg" alt="">
                <h3 class=""><a href="#">Hospital Administration</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/4.jpg" alt="">
                <h3 class=""><a href="#">Dean's Office</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Medical Directors</a></h3>
                <!-- <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit tem autem voluptatem obcaecati.</p>
                <a href="#" class="btn btn-flat btn-theme-colored mt-15 text-theme-color-2">Read More</a> -->             
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/1.jpg" alt="">
                <h3 class=""><a href="#">Nursing Department</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/4.jpg" alt="">
                <h3 class=""><a href="#">Pharmacy Department</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Management Information</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Cardiac Rehabilitation</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Nutrition Department</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Bio Informatics Data</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Quality Assurance</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Bio-Medical Engineering</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Centra Sterilize Services</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Security Department</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Finance Section</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Internal Audit</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Material Management</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Facility Management</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Human Resource</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Out Patient</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Emergency Department</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Cath Lab</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Operation Theaters</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">In Patient</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Liasion Department</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Medical Records</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Board of Governors Office</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Media Department</a></h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
              <div class="p-20 bg-white">
                <img src="images/blog/2.jpg" alt="">
                <h3 class=""><a href="#">Legal Department</a></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>