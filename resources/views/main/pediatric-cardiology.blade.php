  <section class="bg-lighter">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-5">
                @if(count($third_content) > 0)
                    @if( $third_content->first()->content_pic != NULL )
                        <img src="{{ env('APP_CMS') . '/resources/contents/'. $third_content->first()->content_pic }}" alt="">
                    @endif
                @endif
            </div>
            <div class="col-md-7">
                @if(count($third_content) > 0)
                    {!! $third_content->first()->content_description !!}
                @endif
            </div>
            
          </div>
        </div>
      </div>
    </section>
