@extends("template.base")
@section("main-section")
  <div class="main-content">
      
  <section class="inner-header divider parallax layer-overlay overlay-white-4" data-bg-img="{{ url('/resources/images/bg/heart.jpg') }}" style="background-image: url({{ url('/resources/images/bg/heart.jpg') }});background-position: 50% -200px !important;height: 200px">
    <div class="container pt-60 pb-60">
      <!-- Section Content -->
      <div class="section-content">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="title" style="color: white">Details</h2>
          </div>
        </div>
      </div>
    </div>
  </section>

 <section id="depertment" class="bg-silver-light bg-lighter">
      <div class="container">
        <div class="section-content">
          @foreach($doctor as $doc)
          <div class="row">
            <div class="col-md-4">
              <div class="thumb">
                <img src="{{ env('APP_CMS') . '/resources/doctors/' . $doc->doctor_image }}" alt="" style="width:100%;height: 250px">
              </div>
            </div>
            <div class="col-md-8">
              <h3 class="name font-24 mt-0 mb-0">{{ $doc->name }}</h3>
              <h4 class="mt-5 text-theme-colored">{{ $doc->title }}</h4>
              {!! $doc->description !!}
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section>


  </div>
@endsection
