  <section class="inner-header divider parallax layer-overlay overlay-white-4" data-bg-img="{{ url('/resources/tenders.jpg') }}" style="background-image: url({{ url('/resources/tenders.jpg') }});height: 400px">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title" >Tenders</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
  <section id="about bg-lighter" >
      <div class="container-fluid">
        <div class="section-content">
          <div class="row">
            <div class="col-sm-12">
              <center><h2 class="font-size-38 mt-0"><span class="text-theme-colored">Available Tenders</span></h2></center>
              <hr>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <table class="table">
                  <thead>
                  <tr>
                      <th><b>#</b></th>
                      <th><b>Name</b></th>

                      <th><b>Posted On</b></th>
                      <th><b>Closing On</b></th>
                      <th><b>Tender Adv.</b></th>

                      <th><b>SBD</b></th>
                      <th><b>Technical Evaluation</b></th>
                      <th><b>Financial Remarks</b></th>
                      <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach( $tenders as $tend)
                      <tr>
                          <td class="tenderid">{!! $tend->id !!}</td>
                          <td><b>{!! $tend->tender_name !!}</b><br><span style="font-size:12px;">{!! $tend->procurement_entity !!}</span></td>

                          <td>{!! date('d M, Y' , strtotime($tend->advertisement_date)) !!}</td>
                          <td>{!! date('d M, Y' , strtotime($tend->closing_date)) !!}</td>
                          <td>
                            @if($tend->tender_eoi != NULL)
                            <a target="_blank" href="{{ env('APP_CMS') }}/resources/tenders/{{ $tend->tender_eoi }}"><img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" style="width: 20px" ></a>
                            @endif
                            @if($tend->tender_eoi_2 != NULL)
                            <a target="_blank" href="{{ env('APP_CMS') }}/resources/tenders/{{ $tend->tender_eoi_2 }}"><img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" style="width: 20px" ></a>
                            @endif
                          </td>

                          <td>
                            @if($tend->sbd != NULL)
                            <a target="_blank" href="{{ env('APP_CMS') }}/resources/tenders/{{ $tend->sbd }}"><img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" style="width: 20px" ></a>
                            @endif
                          </td>
                          <td>
                            @if($tend->tender_evaluation != NULL)
                            <a target="_blank" href="{{ env('APP_CMS') }}/resources/tenders/{{ $tend->tender_evaluation }}"><img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" style="width: 20px" ></a>
                            @endif
                          </td>
                          <td>{!! $tend->financial_remarks !!}</td>
                          <td><a href="" style="color: blue" data-toggle="modal" data-target="#tenderModel" class="getTenderDetails" >Details</a></td>
                      </tr>
                  @endforeach
                  </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </section>

<div class="modal" id="tenderModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Tender Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <label><b>Description</b></label>
            <textarea class="form-control tenderDescription" rows="10"></textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function(){
      $('.table').DataTable();

      $(".getTenderDetails").on("click" , function(){
        id = $(this).parent().parent().find("td.tenderid").html();
        $.ajax({
            url : "{{ url('/get-tender-details') }}/" + id ,
            type : "GET",
            success : function(data)
            {
                $(".tenderDescription").html(data);
            }
        });
      });
  });
</script>