  <section class="bg-lighter">
      <div class="container">
        <div class="section-content">
          <div class="row">
              <div class="col-md-7">
                @if(count($second_content) > 0)
                    {!! $second_content->first()->content_description !!}
                @endif
            </div>
            <div class="col-md-5">
                @if(count($second_content) > 0)
                    @if( $second_content->first()->content_pic != NULL )
                        <img src="{{ env('APP_CMS') . '/resources/contents/'. $second_content->first()->content_pic }}" style="margin-top:5%" alt="">
                    @endif
                @endif
            </div>
            
          </div>
        </div>
      </div>
    </section>
