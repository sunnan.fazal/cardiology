@extends("template.base")
@section("main-section")
  <div class="main-content">
      
  <section class="inner-header divider parallax layer-overlay overlay-white-4" data-bg-img="{{ url('/resources/aboutus.jpg') }}" style="background-image: url({{ url('/resources/aboutus.jpg') }});background-position: 50% -200px !important;height: 200px">
    <div class="container pt-60 pb-60">
      <!-- Section Content -->
      <div class="section-content">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="title" style="color: #1196CC !important;font-weight: bold">About US</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
 <section class="bg-lighter">
      <div class="container">
        <div class="section-content">
          @foreach($about as $ab)
            <img src="{{ env('APP_CMS') . '/resources/aboutus/' . $ab->banner_image }}" style="width: 100%;height: 500px">
            <br>
            {!! $ab->details !!}
            <br>
            <div class="row">
              @if($ab->image_1 != NULL)
                <div class="col-sm-4">
                  <img src="{{ env('APP_CMS') . '/resources/aboutus/' . $ab->image_1 }}" style="width: 100%;height: 300px">  
                </div>
              @endif
                
                @if($ab->image_2 != NULL)
                  <div class="col-sm-4">
                    <img src="{{ env('APP_CMS') . '/resources/aboutus/' . $ab->image_2 }}" style="width: 100%;height: 300px">  
                  </div>
              @endif
                
                @if($ab->image_3 != NULL)
                  <div class="col-sm-4">
                    <img src="{{ env('APP_CMS') . '/resources/aboutus/' . $ab->image_3 }}" style="width: 100%;height: 300px">  
                  </div>
              @endif
                
            </div>
            <br>
            {!! $ab->details_2 !!}
            <br>
            <div class="row">
              @if($ab->image_4 != NULL)
                <div class="col-sm-4">
                  <img src="{{ env('APP_CMS') . '/resources/aboutus/' . $ab->image_4 }}" style="width: 100%;height: 300px">  
                </div>
              @endif
                
                @if($ab->image_5 != NULL)
                <div class="col-sm-4">
                  <img src="{{ env('APP_CMS') . '/resources/aboutus/' . $ab->image_5 }}" style="width: 100%;height: 300px">  
                </div>
              @endif
                
                @if($ab->image_6 != NULL)
                <div class="col-sm-4">
                  <img src="{{ env('APP_CMS') . '/resources/aboutus/' . $ab->image_6 }}" style="width: 100%;height: 300px">  
                </div>
              @endif
            </div>
            <br>
            {!! $ab->details_3 !!}
            <br>
            <div class="row">
              @if($ab->image_7 != NULL)
              <div class="col-sm-4">
                  <img src="{{ env('APP_CMS') . '/resources/aboutus/' . $ab->image_7 }}" style="width: 100%;height: 300px">  
                </div>
              @endif
                
                @if($ab->image_8 != NULL)
                <div class="col-sm-4">
                  <img src="{{ env('APP_CMS') . '/resources/aboutus/' . $ab->image_8 }}" style="width: 100%;height: 300px">  
                </div>
              @endif
                
                @if($ab->image_9 != NULL)
                <div class="col-sm-4">
                  <img src="{{ env('APP_CMS') . '/resources/aboutus/' . $ab->image_9 }}" style="width: 100%;height: 300px">  
                </div>
              @endif
                
            </div>
          @endforeach
        </div>
      </div>
    </section>


  </div>
@endsection
