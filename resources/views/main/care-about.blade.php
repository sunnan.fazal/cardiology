  <section id="about">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-5">
                @if(count($first_content) > 0)
                    @if( $first_content->first()->content_pic != NULL )
                        <img src="{{ env('APP_CMS') . '/resources/contents/'. $first_content->first()->content_pic }}" style="margin-top:5%" alt="">
                    @endif
                @endif
            </div>
            <div class="col-md-7">
                @if(count($first_content) > 0)
                    {!! $first_content->first()->content_description !!}
                @endif
            </div>
          </div>
        </div>
      </div>
    </section>
