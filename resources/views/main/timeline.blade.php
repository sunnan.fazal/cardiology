<!DOCTYPE html>
<html lang="en">

    <head>
    <header>
        <meta name="viewport" content="width=device-width">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

        <title>Timeline PIC-MTI</title>
        <link rel="stylesheet" href="{{ url('/resources/css/timeline.css') }}">
        <style>
            body{
                font-family: 'Raleway', sans-serif;
            }

            div#timenil{
                position:relative;
                height:100%;
                width:100%;
                display:table;
                /*display: -webkit-flex;
                display: flex;
                -webkit-flex-direction: coloumn;
                flex-direction: coloumn;
                overflow:hidden;*/
            }


            @keyframes fadeIn {
                0% {
                    opacity: 0.5	;
                }
                100% {
                    visibility: visible;
                    opacity: 1;
                }
            }


        </style>
        <script type="text/javascript">
            window.onload = init;
            function init() {
                var x = document.getElementById("timenil");
                var y = x.getElementsByClassName("timenil-node");
                var i;
                for (i = 0; i < y.length; i++) {
                    y[i].style = "animation: 2s fadeIn;animation-fill-mode: forwards;visibility: hidden; animation-delay:" + ((i / 2)) + "s;";
                }
            }
        </script>
    </head>
    <body>
    <center>
        <h1 style="color:#2c3e50;">Peshawar Institute of Cardiology</h1>
        <span style="color:#2c3e50; display:block;">PIC MTI reforms</span><br>
       
    </center>
    <div id="timenil">
        <div class="timenil-vertical-line"></div>
        <div class="timenil-node">
            <div class="timenil-node-separator">
                 Scroll Down
            </div>
        </div>
        @php $i = -1; @endphp        
        @foreach($timeline as $tl)
        @php $i++; @endphp        
        @if($i % 2 == 0)
            <div class="timenil-node">
            <div class="timenil-node-child-left">
                <div class="timenil-content-box">
                    <div class="timenil-content-text">

                        <p></p><h1>{{ $tl->title }}</h1><p></p>
                        <p>{{ $tl->description }}</p>

                    </div>
                </div>
            </div>

            <div class="timenil-node-center"></div>
            <div class="timenil-node-trace"></div>

            <div class="timenil-node-child-right timenil-node-child-right-theme">
                <div class="timenil-content-box">
                    <div class="timenil-content-text">
                        <img src="{{ env('APP_CMS') . '/resources/timeline/' . $tl->image }}" style="width:100%; height:100%;">
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="timenil-node" >
            <div class="timenil-node-child-left timenil-node-child-left-theme">
                <div class="timenil-content-box">
                    <img src="{{ env('APP_CMS') . '/resources/timeline/' . $tl->image }}" style="width:100%; height:100%;">
                </div>
            </div>

            <div class="timenil-node-center"></div>
            <div class="timenil-node-trace"></div>

            <div class="timenil-node-child-right">
                <div class="timenil-content-box">
                    <div class="timenil-content-text">
                        <p></p><h1>{{ $tl->title }}</h1><p></p>
                        <p>{{ $tl->description }}</p>
                    </div>
                </div>
            </div>
        </div>
        @endif
            
        @endforeach
    </div>
</body>
</html>
