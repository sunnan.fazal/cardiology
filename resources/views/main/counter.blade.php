<section class="divider parallax layer-overlay overlay-theme-colored-8" data-bg-img="{{ url('/resources/images/bg/heart.jpg') }}" data-parallax-ratio="0.7">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-smile mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="0" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h4 class="text-white text-uppercase font-weight-600">Happy Patients</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-rocket mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="0" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h4 class="text-white text-uppercase font-weight-600">Our Services</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-add-user mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="0" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h4 class="text-white text-uppercase font-weight-600">Our Doctors</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-global mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="0" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h4 class="text-white text-uppercase font-weight-600">Service Points</h4>
            </div>
          </div>
        </div>
      </div>
    </section>