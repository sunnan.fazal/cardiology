  <section class="inner-header divider parallax layer-overlay overlay-white-5" data-bg-img="{{ url('/resources/downloads.jpg') }}" style="background-image: url({{ url('/resources/downloads.jpg') }});height: 400px">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title" >Downloads</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
  <section id="about bg-lighter">
      <div class="container-fluid">
        <div class="section-content">
          <div class="row">
            <div class="col-sm-12">
              <center><h2 class="font-size-38 mt-0"><span class="text-theme-colored">Download Files</span></h2></center>
              <hr>
            </div>
          </div>
          <div class="row">
            @foreach($downloads as $down)
            <div class="col-sm-3">
              <center>
                  <div class="icon-box iconbox-border iconbox-theme-colored p-40">
                  <a href="{{ env('APP_CMS') }}/resources/downloads/{{ $down->file }}" class="icon icon-gray icon-bordered icon-border-effect effect-flat">
                    <img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" >
                  </a>
                  <h5 class="icon-box-title">{{ $down->name }}</h5>
                  <p class="text-gray">{{ $down->description }}</p>
                  <a class="btn btn-dark btn-sm mt-15" href="{{ env('APP_CMS') }}/resources/downloads/{{ $down->file }}">Download Now</a>
                </div>
              </center>
            </div>
            @endforeach
          </div>
          <hr>
        </div>
      </div>
    </section>
