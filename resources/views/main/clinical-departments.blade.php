   <section>
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase mt-0 line-height-1">CLINICAL DEPARTMENTS</h2>
              <div class="title-icon">
                <img class="mb-10" src="images/title-icon.png" alt="">
              </div>
            </div>
          </div>
        </div>
         <div class="section-content">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="icon-box iconbox-theme-colored2 left media p-0">
                <a href="#" class="media-left pull-left"><img src="{{ url('/resources/images/clinicalicons/baby-boy.png') }}" style="margin-top: 6px"></a>
                <div class="media-body">
                  <h3 class="media-heading heading">Pediatric Cardiac Surgery</h3>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="icon-box iconbox-theme-colored2 left media p-0">
                <a href="#" class="media-left pull-left"><img src="{{ url('/resources/images/clinicalicons/heart.png') }}" style="margin-top: 6px"></a>
                <div class="media-body">
                  <h3 class="media-heading heading">Cardio-Vascular</h3>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum consectetur sit ullam perspiciatis, deserunt.</p> -->
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="icon-box iconbox-theme-colored2 left media p-0">
                <a href="#" class="media-left pull-left"><img src="{{ url('/resources/images/clinicalicons/anesthesia.png') }}" style="margin-top: 6px"></a>
                <div class="media-body">
                  <h3 class="media-heading heading">Anesthesia Department</h3>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum consectetur sit ullam perspiciatis, deserunt.</p> -->
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="icon-box iconbox-theme-colored2 left media p-0">
                <a href="#" class="media-left pull-left"><img src="{{ url('/resources/images/clinicalicons/blood-sample.png') }}" style="margin-top: 6px"></a>
                <div class="media-body">
                  <h3 class="media-heading heading">Pathology Department</h3>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="icon-box iconbox-theme-colored2 left media p-0">
                <a href="#" class="media-left pull-left"><img src="{{ url('/resources/images/clinicalicons/x-ray.png') }}" style="margin-top: 6px"></a>
                <div class="media-body">
                  <h3 class="media-heading heading">Radiology Department</h3>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="icon-box iconbox-theme-colored2 left media p-0">
                <a href="#" class="media-left pull-left"><img src="{{ url('/resources/images/clinicalicons/medicine.png') }}" style="margin-top: 6px"></a>
                <div class="media-body">
                  <h3 class="media-heading heading">General Medicine</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
