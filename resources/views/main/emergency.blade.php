<section  class="divider parallax layer-overlay overlay-theme-colored-8" data-bg-img="{{ url('/resources/images/bg/bg6.jpg') }}">
      <div class="container">
        <div class="call-to-action">
          <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
              <h2 class="text-white"><i class="pe-7s-call text-white"></i><a class="text-white" href="#"> (+92) &ndash; 91 xxx xxxx</a></h2>
              <h2 class="text-white">Please free to contact us for emergency case.</h2>
              <p class="text-white">24/7 Available</p>
              <a href="#" class="btn btn-default btn-theme-colored2 mt-20">Contact With Us</a>
            </div>
          </div>
        </div>
      </div>
    </section>