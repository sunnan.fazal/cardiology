@extends("template.base")
@section("main-section")
  <div class="main-content">
      
  <section class="inner-header divider parallax layer-overlay overlay-white-4" data-bg-img="{{ url('/resources/images/bg/heart.jpg') }}" style="background-image: url({{ url('/resources/images/bg/heart.jpg') }});background-position: 50% -200px !important;height: 200px">
    <div class="container pt-60 pb-60">
      <!-- Section Content -->
      <div class="section-content">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="title" style="color: white">Details</h2>
          </div>
        </div>
      </div>
    </div>
  </section>

 <section id="depertment" class="bg-silver-light bg-lighter">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <img class="pull-left flip mr-15 thumbnail" src="{{ env('APP_CMS') }}/resources/news/{{ $news->news_image }}" style="height: 350px" alt="">
            <p>{!! $news->news_image_description !!}</p>
            <div class="clearfix"></div>
            <p>{!! $news->news_details !!}</p>
            <div class="separator separator-rouned">
              <i class="fa fa-cog fa-spin"></i>
            </div>
            <div class="row">
              @if($news->news_image_1 != NULL)
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img style="height: 300px" alt="..." src="{{ env('APP_CMS') }}/resources/news/{{ $news->news_image_1 }}" class="img-fullwidth"> </a> </div>
              @endif
              @if($news->news_image_2 != NULL)
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img style="height: 300px" alt="..." src="{{ env('APP_CMS') }}/resources/news/{{ $news->news_image_2 }}" class="img-fullwidth"> </a> </div>
              @endif
              @if($news->news_image_3 != NULL)
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img style="height: 300px" alt="..." src="{{ env('APP_CMS') }}/resources/news/{{ $news->news_image_3 }}" class="img-fullwidth"> </a> </div>
              @endif
              @if($news->news_image_4 != NULL)
              <div class="col-xs-6 col-md-3"> <a class="thumbnail" href="#"> <img style="height: 300px" alt="..." src="{{ env('APP_CMS') }}/resources/news/{{ $news->news_image_4 }}" class="img-fullwidth"> </a> </div>
              @endif
            </div>
            <div class="separator separator-rouned">
              <i class="fa fa-cog"></i>
            </div>
            <p>{!! $news->news_last_description !!}</p>
          </div>
        </div>
      </div>
    </section>


  </div>
@endsection
