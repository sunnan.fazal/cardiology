<section id="doctors">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class=" mt-0 line-height-1">Our <span class="text-theme-colored">Consultants</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="images/title-icon.png" alt="">
              </div>
              <p>PIC Consultants team</p>
            </div>
          </div>
        </div>
        <div class="row mtli-row-clearfix">
          <div class="col-md-12">
            <div class="owl-carousel-4col">
              @foreach($doctors as $doc)
              <div class="item">
                <a href="{{ url('/doctor-details/' . $doc->id) }}">
                <div class="team-members border-bottom-theme-color-2px text-center maxwidth400">
                  <div class="team-thumb">
                    <img class="img-fullwidth" alt="" src="{{ env('APP_CMS') . '/resources/doctors/' . $doc->doctor_image }}" style="height: 200px">
                    <div class="team-overlay"></div>
                  </div>
                  <div class="team-details bg-silver-light pt-10 pb-10">
                    <h4 class="text-uppercase font-weight-600 m-5">{{ $doc->name }}</h4>
                    <h6 class="text-theme-colored font-15 font-weight-400 mt-0">{{ $doc->title }}</h6>
                  </div>
                </div>
                </a>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>