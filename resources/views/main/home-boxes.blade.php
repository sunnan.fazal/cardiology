  <section>
      <div class="container pt-0 pb-0">
        <div class="section-content">
          <div class="row equal-height-inner mt-sm-0" data-margin-top="-110px" style="margin-top: -110px;">
            
            <div class="col-sm-12 col-md-3 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay1">
              <div class="sm-height-auto bg-theme-colored">
                <div class="p-30">
                  <a href="{{ url('/about-us') }}">
                  <h3 class="text-uppercase text-white mt-0">About PIC-MTI <!--<br><small class="text-gray-lighter">Care Solutions</small>--></h3>
                  <!--<p class="text-white">Quality Affordable In-Home Care In Your Community Is Just A Phone Call Away.&nbsp; Learn More About Us Below.</p>
                  <a href="#about" class="btn btn-border btn-circled btn-transparent btn-sm">About Us</a>-->
                  </a>
                </div>
              </div>
            </div>

            <div class="col-sm-12 col-md-3 pl-0 pl-sm-15 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay2">
              <div class="sm-height-auto bg-theme-colored2">
                <div class="p-30">
                  <h3 class="text-uppercase text-white mt-0">Clinical Departments</h3>
                  <!--<div class="opening-hours">
                    <ul class="list-unstyled text-white">
                      <li class="clearfix"> <span>Monday</span>
                        <div class="value">8:00am - 4:30pm </div>
                      </li>
                      <li class="clearfix"> <span>Tues - Thur</span>
                        <div class="value">8:00am - 4:30pm</div>
                      </li>
                      <li class="clearfix"> <span>Friday</span>
                        <div class="value">8:00am - 4:30pm</div>
                      </li>
                    </ul>
                  </div>-->
                <!--  <a class="btn btn-border btn-circled btn-transparent btn-sm mt-20" data-toggle="modal" data-target="#BSParentModal" href="ajax-load/form-appointment.html">Request an appointment</a>-->
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-md-3 pl-0 pr-0 pl-sm-15 0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay3">
              <div class="sm-height-auto bg-theme-colored">
                <div class="p-30">
                  <h3 class="text-uppercase text-white mt-0">Administrative Services <!--<br><small class="text-gray-lighter">Defined By You</small>--></h3>
                  <!--<p class="text-white">Care Management, Personal Assistance, Housekeeping, Nutritional Management, Medication Assistance</p>
                  <a href="#services" class="btn btn-border btn-circled btn-transparent btn-sm">Learn More</a>-->
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-md-3 pl-0 pl-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay4">
              <div class="sm-height-auto bg-theme-colored2">
                <div class="p-30">
                  <h3 class="text-uppercase text-white mt-0">MTI Act Rules & Regulations <!--<br><small class="text-gray-lighter">Close To Home</small>--></h3>
                  <h2 class="text-white"></h2>
                  <!--<p class="text-white">Get The Quality Care That You Deserve Immediately</p>
                  <a href="page-contact1.html" class="btn btn-border btn-circled btn-transparent btn-sm">Contact Us</a>-->
                  <a class="btn btn-border btn-circled btn-transparent btn-sm mt-20" href="{{ url('/MTI-act-rules-regulations') }}">DOWNLOAD HERE</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>