<section id="blog">
      <div class="container pb-50">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="mt-0 line-height-1">Recent  <span class="text-theme-colored"> News</span></h2>
              <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p> -->
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="owl-carousel-3col owl-nav-top mb-sm-0" data-nav="true">
                @foreach($news as $new)
                   <div class="item">
                    <article class="post clearfix maxwidth600 mb-30 wow fadeInRight" data-wow-delay=".2s">
                      <div class="entry-header">
                        <div class="post-thumb thumb"> 
                          <img src="{{ env('APP_CMS') }}/resources/news/{{ $new->news_image }}" alt="" class="img-responsive img-fullwidth" style="    height: 250px;"> 
                        </div>
                      </div>
                      <div class="entry-content bg-lighter p-20 pr-10">
                        <div class="entry-meta mt-0 no-bg no-border">
                          <div class="event-content">
                            <h3 class="entry-title text-white text-capitalize m-0"><a href="{{ url('news-details/' . $new->id) }}">{!! substr($new->news_image_description ,0 , 50 ) !!}</a></h3>
                          </div>
                        </div>
                        <div class="mt-10"> <a href="{{ url('news-details/' . $new->id) }}" class="btn btn-theme-colored btn-sm">Read More</a> </div>
                        <div class="clearfix"></div>
                      </div>
                    </article>
                  </div>
                @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>