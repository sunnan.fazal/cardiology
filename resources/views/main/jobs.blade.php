  <section class="inner-header divider parallax layer-overlay overlay-white-4" data-bg-img="{{ url('/resources/careers.png') }}" style="background-image: url({{ url('/resources/careers.png') }});height: 400px">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title" >Careers</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
  <section id="about bg-lighter" >
  <div class="container-fluid">
    <div class="section-content">
      <div class="row">
        <div class="col-sm-12">
          <center><h2 class="font-size-38 mt-0"><span class="text-theme-colored">Available Jobs</span></h2></center>
          <hr>
        </div>
      </div>
      <hr>
      <div class="row">
            <div class="col-sm-12">
              <table class="table">
                  <thead>
                  <tr>
                      <th><b>Job Id</b></th>
                      <th><b>Description</b></th>
                      <th><b>Type</b></th>
                      <th><b>Posted On</b></th>
                      <th><b>Closing On</b></th>
                      <th><b>Attachment</b></th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach( $careers as $tend)
                      <tr>
                          <td class="careerid">{!! $tend->id !!}</td>
                          <td>{!! $tend->description !!}</td>
                          <td>{!! $tend->type !!}</td>
                          <td>{!! date('d M, Y' , strtotime($tend->advertisement_date)) !!}</td>
                          <td>{!! date('d M, Y' , strtotime($tend->closing_date)) !!}</td>
                          <td>
                            @if($tend->file != NULL)
                              <a target="_blank" href="{{ env('APP_CMS') }}/resources/careers/{{ $tend->file }}"><img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" style="width: 20px" ></a>
                            @endif
                            </td>
                      </tr>
                  @endforeach
                  </tbody>
              </table>
            </div>
          </div>

    </div>
  </div>
</section>
<script>
  $(document).ready(function(){
      $('.table').DataTable();
  });
</script>