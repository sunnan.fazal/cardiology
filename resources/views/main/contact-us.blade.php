@extends("template.base")
@section("main-section")
<div class="main-content">      
  
<section class="inner-header divider parallax layer-overlay overlay-white-4" data-bg-img="{{ url('/resources/contactus.jpg') }}" style="background-image: url({{ url('/resources/contactus.jpg') }});background-position: 50% -200px !important;height: 400px">
  <div class="container pt-60 pb-60">
    <!-- Section Content -->
    <div class="section-content">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2 class="title" style="color: black">Contact Us</h2>
         
        </div>
      </div>
    </div>
  </div>
</section>

<section id="about" class="bg-lighter" >
  <div class="container">
    <div class="row pt-30">
      <div class="col-md-4">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-map-2 text-theme-colored"></i></a>
              <div class="media-body">
                <h5 class="mt-0">Hospital Location</h5>
                <p>5-A, Sector B-3, Phase-V, Hayatabad, Peshawar - Pakistan</p>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-12">
            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-call text-theme-colored"></i></a>
              <div class="media-body">
                <h5 class="mt-0">Contact Number</h5>
                <p>+xxx xxx xxxx</p>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-12">
            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-mail text-theme-colored"></i></a>
              <div class="media-body">
                <h5 class="mt-0">Email Address</h5>
                <p>info@pic.edu.pk</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <h3 class="line-bottom mt-0 mb-30">Interested in discussing?</h3>
        <!-- Contact Form -->
        <form id="contact_form" name="contact_form" class="" action="includes/sendmail.php" method="post" novalidate="novalidate">

          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label>Name <small>*</small></label>
                <input name="form_name" class="form-control" type="text" placeholder="Enter Name" required="" aria-required="true">
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label>Email <small>*</small></label>
                <input name="form_email" class="form-control required email" type="email" placeholder="Enter Email" aria-required="true">
              </div>
            </div>
          </div>
            
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Subject <small>*</small></label>
                <input name="form_subject" class="form-control required" type="text" placeholder="Enter Subject" aria-required="true">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Phone</label>
                <input name="form_phone" class="form-control" type="text" placeholder="Enter Phone">
              </div>
            </div>
          </div>

          <div class="form-group">
            <label>Message</label>
            <textarea name="form_message" class="form-control required" rows="5" placeholder="Enter Message" aria-required="true"></textarea>
          </div>
          <div class="form-group">
            <input name="form_botcheck" class="form-control" type="hidden" value="">
            <button type="submit" class="btn btn-dark btn-theme-colored btn-flat mr-5" data-loading-text="Please wait...">Send your message</button>
            <button type="reset" class="btn btn-default btn-flat btn-theme-colored">Reset</button>
          </div>
        </form>

        <!-- Contact Form Validation-->
        <script>
          $("#contact_form").validate({
            submitHandler: function(form) {
              var form_btn = $(form).find('button[type="submit"]');
              var form_result_div = '#form-result';
              $(form_result_div).remove();
              form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
              var form_btn_old_msg = form_btn.html();
              form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
              $(form).ajaxSubmit({
                dataType:  'json',
                success: function(data) {
                  if( data.status === 'true' ) {
                    $(form).find('.form-control').val('');
                  }
                  form_btn.prop('disabled', false).html(form_btn_old_msg);
                  $(form_result_div).html(data.message).fadeIn('slow');
                  setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                }
              });
            }
          });
        </script>
      </div>
    </div>
  </div>
</section>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3308.14489307152!2d71.43692121306184!3d33.988811897198474!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf8fe581527e6059f!2sPeshawar%20Institute%20of%20Cardiology!5e0!3m2!1sen!2s!4v1594582148095!5m2!1sen!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
@endsection