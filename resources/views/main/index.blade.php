@extends("template.base")
@section("main-section")
  <div class="main-content">
  		@include("main.slider")
  		@include("main.home-boxes")
  		@include("main.care-about")
  		@include("main.welcome")
  		@include("main.clinical-departments")
      @include("main.pediatric-cardiology")
  		@include("main.counter")
  		@include("main.consultants")
  		@include("main.emergency")
  		@include("main.gallery")
  		@include("main.news")
  </div>
@endsection