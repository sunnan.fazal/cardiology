@extends("template.base")
@section("main-section")
  <div class="main-content">
      
  <section class="inner-header divider parallax layer-overlay overlay-white-4" data-bg-img="{{ url('/resources/board.jpg') }}" style="background-image: url({{ url('/resources/board.jpg') }});height: 400px">
    <div class="container pt-60 pb-60">
      <!-- Section Content -->
      <div class="section-content">
        <div class="row">
          <div class="col-md-12 text-center">
          </div>
        </div>
      </div>
    </div>
  </section>
 <section class="bg-lighter">
      <div class="container">
        <div class="section-content">
          <h1 style="font-family: Raleway">The Board of Governors</h1>
          <hr>
          <p style="font-size: 20px;color: black">The Board of Governors are a dynamic group of leaders and key decision-makers, who are responsible for making the best decisions for the good of the Hospital’s patients and employees. The Board is supported by a larger executive committee who then work closely with the core management teams to achieve the goals as per the vision and mission of the Hospital.</p>
        </div>
        <hr>
        <div class="row clearfix">
          @foreach($bog as $bg)
            <div class="col-sm-4 sm-text-center mb-30 mb-sm-30">
              <div class="team-members border-bottom-theme-color-2px maxwidth400">
                <div class="team-thumb">
                  <img class="img-fullwidth" alt="" src="{{ env('APP_CMS') . '/resources/bog/' . $bg->bog_image }}" style="height: 300px"> 
                  <div class="team-overlay"></div>
                </div>
                <div class="team-details bg-theme-colored2 p-20">
                  <h4 class="text-uppercase mb-0"><a class="text-white" href="#">{{ $bg->bog_name }}</a></h4>
                  <p style="color: lightblue">{{ $bg->bog_title }}</p>
                  <!-- <ul class="styled-icons icon-theme-colored icon-dark icon-circled icon-sm">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                  </ul> -->
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </section>
  </div>
@endsection
