  <section class="inner-header divider parallax layer-overlay overlay-white-4" data-bg-img="{{ url('/resources/rules-and-regulations.jpg') }}" style="background-image: url({{ url('/resources/tenders.jpg') }});height: 400px">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title" >MTI Acts Rules & Regulations</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
  <section id="about bg-lighter" >
      <div class="container-fluid">
        <div class="section-content">
          <div class="row">
            <div class="col-sm-12">
              <center><h2 class="font-size-38 mt-0"><span class="text-theme-colored">MTI Rules & Regulations Documents</span></h2></center>
              <hr>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <table class="table">
                  <thead>
                  <tr>
                      <th><b>#</b></th>
                      <th><b>Description</b></th>
                      <th>Attachment</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($mti_act as $act)
                      <tr>
                          <td>{!! $act->id !!}</td>
                          <td>{!! $act->description !!}</td>
                          <td>
                            @if($act->file != NULL)
                            <a target="_blank" href="{{ env('APP_CMS') }}/resources/mti_acts/{{ $act->file }}"><img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" style="width: 20px" ></a>
                            @endif
                          </td>
                      </tr>
                    @endforeach
                  </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </section>

<script>
  $(document).ready(function(){
      $('.table').DataTable();
  });
</script>

<!--   <section id="about">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="font-size-38 mt-0"><span class="text-theme-colored">MTI ACT RULES & REGULATIONS Documents</span></h2>
              <hr>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
                <img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" width="25px" height="25px;">
                <a href="{{ url('/resources/acts/a1.pdf') }}" target="_blank">MTI ACT 2015</a>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-12">
                <img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" width="25px" height="25px;">
                <a href="{{ url('/resources/acts/a2.pdf') }}" target="_blank">AMENDMENT-1</a>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-12">
                <img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" width="25px" height="25px;">
                <a href="{{ url('/resources/acts/a3.pdf') }}" target="_blank">AMENDMENT-2</a>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-12">
                <img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" width="25px" height="25px;">
                <a href="{{ url('/resources/acts/a4.pdf') }}" target="_blank">AMENDMENT-3</a>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-12">
                <img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" width="25px" height="25px;">
                <a href="{{ url('/resources/acts/a5.pdf') }}" target="_blank">Rules</a>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-12">
                <img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" width="25px" height="25px;">
                <a href="{{ url('/resources/acts/bylaw.pdf') }}" target="_blank">By Law Of The Medical Staff PIC 2018</a>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-12">
                <img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" width="25px" height="25px;">
                <a href="{{ url('/resources/acts/handbook.pdf') }}" target="_blank">Institute Employee Hand Book Jan 2018</a>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-12">
                <img src="{{ url('/resources/images/pdfimg.png') }}" alt="peshawar-institute-of-cardiology" width="25px" height="25px;">
                <a href="{{ url('/resources/acts/regulations.pdf') }}" target="_blank">Regulation For PIC Final</a>
            </div>
          </div>
          <hr>



        </div>
      </div>
    </section> -->
