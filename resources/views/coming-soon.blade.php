@extends("template.base")
@section("main-section")
  <div class="main-content">
    <section id="home" class="bg-lightest fullscreen" style="height: 436px;">
      <div class="display-table text-center">
        <div class="display-table-cell">
          <div class="container pt-0 pb-0"><div class="row">
              <div class="col-md-10 col-md-offset-1">
                <h1 class="text-theme-colored font-weight-100 font-64">We Will Be LIVE Soon !</h1>                
                
               <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                    <div id="countdown17" class="ClassyCountdownDemo"><div class="ClassyCountdown-wrapper"><div class="ClassyCountdown-days" style="height: 135px;"><span class="ClassyCountdown-value" style="font-family: &quot;Open Sans&quot;; font-weight: 300; color: rgb(52, 73, 94); margin-top: -34px; font-size: 31px;"><div>0</div><span style="font-size: 0.5em;">Days</span></span></div><div class="ClassyCountdown-hours" style="height: 135px;"><span class="ClassyCountdown-value" style="font-family: &quot;Open Sans&quot;; font-weight: 300; color: rgb(52, 73, 94); margin-top: -34px; font-size: 31px;"><div>2</div><span style="font-size: 0.5em;">Hours</span></span></div><div class="ClassyCountdown-minutes" style="height: 135px;"><span class="ClassyCountdown-value" style="font-family: &quot;Open Sans&quot;; font-weight: 300; color: rgb(52, 73, 94); margin-top: -34px; font-size: 31px;"><div>32</div><span style="font-size: 0.5em;">Minutes</span></span></div><div class="ClassyCountdown-seconds" style="height: 135px;"><span class="ClassyCountdown-value" style="font-family: &quot;Open Sans&quot;; font-weight: 300; color: rgb(52, 73, 94); margin-top: -34px; font-size: 31px;"><div>53</div><span style="font-size: 0.5em;">Seconds</span></span></div></div></div>
                                    
                    <link href="{{ url('/resources/js/classycountdown/css/jquery.classycountdown.css') }}" rel="stylesheet" type="text/css">
                    <script src="{{ url('/resources/js/classycountdown/js/jquery.knob.js') }}"></script> 
                    <script src="{{ url('/resources/js/classycountdown/js/jquery.throttle.js') }}"></script> 
                    <script src="{{ url('/resources/js/classycountdown/js/jquery.classycountdown.js') }}"></script> 

                    <!-- Classy Countdown Script -->
                    <script>
                      $(document).ready(function() {
                        $('#countdown17').ClassyCountdown({
                            theme: "flat-colors-very-wide",
                            end: $.now() + 10000
                        });
                      });
                    </script>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection

                    