@php 
$l_news = App\News::where('active' , 1)->orderByDesc('updated_at')->limit(5)->get(); 
@endphp
<footer id="footer" class="footer bg-black-111">
     <div class="container pt-70 pb-40">
      <div class="row border-bottom-black">
        <div class="col-sm-7 col-md-4">
          <div class="widget dark">
            <img class="mt-10 mb-20" alt="" src="{{ url('/resources/images/logo-1.png')}}">
            <p>PIC Hospital has 250 beds, including beds for Cardiac Surgery (paediatrics and adults), Cardiology (paediatrics and adults), Intensive Care Unit and Cardiac Care Unit.</p>
            <ul class="list-inline mt-5">
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-map-marker text-theme-colored mr-5"></i> <a class="text-gray" href="#">Peshwar Institute of Cardiology - MTI,
5-A, Sector B-3, Phase -V, Hayatabad Peshawar, KP, Pakistan</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-colored mr-5"></i> <a class="text-gray" href="#">xxx-xxx-xxx</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-colored mr-5"></i> <a class="text-gray" href="#">info@pic.edu.pk</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-theme-colored mr-5"></i> <a class="text-gray" href="http://pic.edu.pk">www.pic.edu.pk</a> </li>
            </ul>
          </div>
          <div class="widget dark">
            <h5 class="widget-title mb-10">Connect With Us</h5>
            <ul class="styled-icons icon-dark icon-circled icon-sm">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-skype"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram"></i></a></li>
              <li><a href="https://www.linkedin.com/company/picofficial/"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-5 col-md-2">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Useful Links</h5>
            <ul class="list-border">
              <li><a href="{{ url('/') }}">Home</a></li>
              <li><a href="{{ url('/about-us') }}">About us</a></li>
              <li><a href="{{ url('/contact-us') }}">Contact</a></li>
            </ul>
          </div>
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Other Links</h5>
            <ul class="list-border">
              <li><a href="#">FAQ</a></li>
              <li><a href="#">Sitemap</a></li>
              <li><a href="#">Policy</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Latest News</h5>
            <div class="latest-posts">
              @foreach($l_news as $lnew)
                <article class="post media-post clearfix pb-0 mb-10">
                  <a href="{{ url('news-details/' .$lnew->id) }}" class="post-thumb"><img alt="" src="{{ env('APP_CMS') }}/resources/news/{{ $lnew->news_image }}" style="width: 80px;
height: 60px;"></a>
                  <div class="post-right">
                    <h5 class="post-title mt-0 mb-5"><a href="{{ url('news-details/' .$lnew->id) }}">{!! substr(strip_tags(htmlspecialchars_decode($lnew->news_image_description)) ,0 , strpos($lnew->news_image_description, ' ', 50) ) !!}</a></h5>
                    <p class="post-date mb-0 font-12">{!! date('M d, Y' , strtotime($lnew->updated_at)) !!}</p>
                  </div>
                </article>
              @endforeach
            </div>
          </div>
          <!-- <div class="widget dark">
            <h5 class="widget-title line-bottom">Call Us Now</h5>
            <div class="text-gray">
              +xx xxx xxxx <br>
              +xx xxx xxxx
            </div>
          </div> -->
        </div>
        <div class="col-sm-6 col-md-3">
          <!-- <div class="widget dark">
            <h5 class="widget-title line-bottom">Opening Hours</h5>
            <div class="opening-hours">
              <ul class="list-border">
                <li class="clearfix"> <span> Mon :  </span>
                  <div class="value pull-right flip"> 8.00 am - 4.30 pm </div>
                </li>
                <li class="clearfix"> <span> Tues </span>
                  <div class="value pull-right flip"> 8.00 am - 4.30 pm </div>
                </li>
                <li class="clearfix"> <span> Wed : </span>
                  <div class="value pull-right flip"> 8.00 am - 4.30 pm </div>
                </li>
                <li class="clearfix"> <span> Thurs : </span>
                  <div class="value pull-right flip"> 8.00 am - 4.30 pm</div>
                </li>
                <li class="clearfix"> <span> Fri : </span>
                  <div class="value pull-right flip"> 8.00 am - 4.30 pm </div>
                </li>
              </ul>
            </div>
          </div> -->
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Subscribe Us</h5>
            <!-- Mailchimp Subscription Form Starts Here -->
            <form id="mailchimp-subscription-form-footer" class="newsletter-form" novalidate="true">
              <div class="input-group">
                <input type="email" value="" name="EMAIL" placeholder="Your Email" class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-footer" style="height: 45px;">
                <span class="input-group-btn">
                  <button data-height="45px" class="btn btn-colored btn-theme-colored btn-xs m-0 font-14" type="submit" style="height: 45px;">Subscribe</button>
                </span>
              </div>
            </form>
            <!-- Mailchimp Subscription Form Validation-->
            <script>
              $('#mailchimp-subscription-form-footer').ajaxChimp({
                  callback: mailChimpCallBack,
                  url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
              });

              function mailChimpCallBack(resp) {
                  // Hide any previous response text
                  var $mailchimpform = $('#mailchimp-subscription-form-footer'),
                      $response = '';
                  $mailchimpform.children(".alert").remove();
                  if (resp.result === 'success') {
                      $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  } else if (resp.result === 'error') {
                      $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  }
                  $mailchimpform.prepend($response);
              }
            </script>
            <!-- Mailchimp Subscription Form Ends Here -->
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom bg-black-222">
      <div class="container pt-10 pb-0">
        <div class="row">
          <div class="col-md-6 sm-text-center">
            <p class="font-13 text-black-777 m-0">Copyright &copy;2020 Peshawar Institute of Cardiology - MTI. All Rights Reserved.</p>
          </div>
          <div class="col-md-6 text-right flip sm-text-center">
            <div class="widget no-border m-0">
              <ul class="styled-icons icon-dark icon-circled icon-sm">
                <li><a href="https://www.linkedin.com/company/picofficial/"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
	@include("template.files_footer")
</body>
</html>