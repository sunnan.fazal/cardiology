<link href="{{ url('/resources/images/logo.jpg') }}" rel="shortcut icon" type="image/png">
<link href="{{ url('/resources/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css">
<link href="{{ url('/resources/css/jquery-ui.min.css') }} " rel="stylesheet" type="text/css">
<link href="{{ url('/resources/css/animate.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('/resources/css/css-plugin-collections.css') }}" rel="stylesheet"/>
<link href="{{ url('/resources/css/menuzord-megamenu.css') }}" rel="stylesheet"/>
<link id="menuzord-menu-skins" href="{{ url('/resources/css/menuzord-skins/menuzord-boxed.css') }}" rel="stylesheet"/>
<link href="{{ url('/resources/css/style-main.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="{{ url('/resources/css/preloader.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="{{ url('/resources/css/custom-bootstrap-margin-padding.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="{{ url('/resources/css/responsive.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<link  href="{{ url('/resources/js/revolution-slider/css/settings.css') }}" rel="stylesheet" type="text/css"/>
<link  href="{{ url('/resources/js/revolution-slider/css/layers.css') }}" rel="stylesheet" type="text/css"/>
<link  href="{{ url('/resources/js/revolution-slider/css/navigation.css') }}" rel="stylesheet" type="text/css"/>

<!-- CSS | Theme Color -->
<link href="{{ url('/resources/css/colors/theme-skin-color-set1.css')}}" rel="stylesheet" type="text/css">
<link href="{{ url('/resources/css/datatable.min.css')}}" rel="stylesheet" type="text/css">

<script src="{{ url('/resources/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ url('/resources/js/jquery-ui.min.js') }}"></script>
<script src="{{ url('/resources/js/bootstrap.min.js') }}"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="{{ url('/resources/js/jquery-plugin-collection.js') }}"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="{{ url('/resources/js/revolution-slider/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ url('/resources/js/revolution-slider/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ url('/resources/js/datatable.min.js') }}"></script>
