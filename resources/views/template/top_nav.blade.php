@php 
$latest_news = App\News::where('active' , 1)->where("featured_news" , 0)->where('covid_news' , 0)->orderByDesc('updated_at')->limit(3)->get(); 
$featured_news = App\News::where('active' , 1)->where("featured_news" , 1)->orderByDesc('updated_at')->limit(1)->get();
$covid = App\News::where('active' , 1)->where("covid_news" , 1)->orderByDesc('updated_at')->limit(1)->get();
@endphp
  <header id="header" class="header">
    <div class="header-top bg-theme-colored sm-text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="widget text-white">
              <!-- <i class="fa fa-clock-o text-white"></i> Schedule: Monday - Friday- 8:00am - 4:30pm -->
            </div>
          </div>
          <div class="col-md-4">
             <div class="widget">
              <ul class="list-inline text-right flip sm-text-center">
                <li>
                  <a class="text-white" href="{{ url('/downloads') }}"><i class="fa fa-download"></i> Downloads</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="{{ url('/contact-us') }}"><i class="fa fa-phone"></i> Contact Us</a>
                </li>
                <li class="text-white">|</li>
                <li>
                  <a class="text-white" href="https://portal.office.com"><i class="fa fa-envelope-o"></i> Email</a>
                </li>
              </ul>
            </div> 
          </div>
        </div>
      </div>
    </div>
    
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-white" style="z-index: 999;">
        <div class="container">
          <nav id="menuzord-right" class="menuzord blue no-bg menuzord-responsive"><a style="margin-top: 3px;margin-bottom: 3px" class="menuzord-brand pull-left flip" href="{{ url('/') }}"><img src="{{ url('/resources/logo.jpeg')}}" alt="" style="max-height: 80px;"></a>
            <ul class="menuzord-menu menuzord-right menuzord-indented scrollable" style="max-height: 400px;margin-top: 0.5em">
              <li><a href="{{ url('/') }}"><i class="fa fa-home" style="font-size: 30px;"></i><span class="indicator"></span></a>
                <div class="megamenu" style="right: 0px; display: none;">
                  <div class="megamenu-row">
                    <div class="col3">
                      <h4 class="megamenu-col-title">Latest News:</h4>
                      <div class="widget">
                        <div class="latest-posts">
                           @foreach($latest_news as $lnew)
                            <article class="post media-post clearfix pb-0 mb-10">
                              <a href="{{ url('news-details/' .$lnew->id) }}" class="post-thumb"><img alt="" src="{{ env('APP_CMS') }}/resources/news/{{ $lnew->news_image }}" style="width: 80px;
      height: 60px;"></a>
                              <div class="post-right">
                                <h5 class="post-title mt-0 mb-5"><a href="{{ url('news-details/' .$lnew->id) }}">{!! substr(strip_tags(htmlspecialchars_decode($lnew->news_image_description)) ,0 , strpos($lnew->news_image_description, ' ', 45) ) !!}</a></h5>
                              </div>
                            </article>
                          @endforeach
                        </div>
                      </div>
                    </div>
                    <div class="col3">
                      <h4 class="megamenu-col-title"><strong>Featured News:</strong></h4>
                      @foreach($featured_news as $fNew)
                            <article class="post clearfix">
                        <div class="entry-header">
                          <div class="post-thumb"> <img class="img-responsive" style="width: 250px;height: 150px" src="{{ env('APP_CMS') }}/resources/news/{{ $fNew->news_image }}" alt=""> </div>
                        </div>
                        <div class="entry-content">
                          <p class="">{!! substr(strip_tags(htmlspecialchars_decode($fNew->news_image_description)) ,0 , strpos($fNew->news_image_description, ' ', 50) ) !!}</p>
                          <a class="btn btn-dark btn-theme-colored" href="{{ url('news-details/' .$fNew->id) }}">read more..</a> </div>
                      </article>
                      @endforeach
                    </div>
                    <div class="col3">
                      <h4 class="megamenu-col-title"><strong>COVID-19 Updates:</strong></h4>
                       @foreach($covid as $cNew)
                            <article class="post clearfix">
                        <div class="entry-header">
                          <div class="post-thumb"> <img class="img-responsive" style="width: 250px;height: 150px" src="{{ env('APP_CMS') }}/resources/news/{{ $cNew->news_image }}" alt=""> </div>
                        </div>
                        <div class="entry-content">
                          <p class="">{!! substr(strip_tags(htmlspecialchars_decode($cNew->news_image_description)) ,0 , strpos($cNew->news_image_description, ' ', 50) ) !!}</p>
                          <a class="btn btn-dark btn-theme-colored" href="{{ url('news-details/' .$cNew->id) }}">read more..</a> </div>
                      </article>
                      @endforeach
                    </div>
                    <div class="col3">
                      <h4 class="megamenu-col-title">Quick Links:</h4>
                      <ul class="list-dashed list-icon">
                        <li><a href="#">Find a doctor</a></li>
                        <li><a href="#">Diagnostic Services</a></li>
                        <li><a href="{{ url('/timeline') }}">Timeline</a></li>
                        <li><a href="#">Food & Nutrition</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li><a href="{{ url('/about-us') }}">About Us<span class="indicator"></span></a>
                <ul class="dropdown" style="right: auto; display: none;">
                      <li><a href="{{ url('/about-us') }}">About PIC</a></li>
                      <li><a href="{{ url('/governors-board') }}">Board of Governors</a></li>
                      <li><a href="{{ url('/vision-mission') }}">Vision & Mission</a></li>
                </ul>
              </li>
              <li><a href="#">Management<span class="indicator"></span></a>
                <ul class="dropdown" style="right: auto; display: none;">
                      <li><a href="{{ url('/coming-soon') }}">Building & Facilities</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Biomedical Engineering</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Finance</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Human Resource</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Information & Communication Technology</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Material Management</a></li>
                </ul>
              </li>

              <li><a href="{{ url('/services') }}">Services<span class="indicator"></span></a>
                <ul class="dropdown" style="right: auto; display: none;">
                      <li><a href="{{ url('/coming-soon') }}">Emergency & Trauma</a></li>
                      <li><a href="{{ url('/coming-soon') }}">In-Patient</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Interventional Radiology</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Out Patient</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Pathology</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Pharmacy & Stores</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Physiotherapy</a></li>
                      <li><a href="{{ url('/coming-soon') }}">Radiology</a></li>
                </ul>
              </li>
              <li><a href="{{ url('/tenders') }}">Tenders</a></li>
              <li><a href="{{ url('/careers') }}">Careers</a></li>
            </ul>
          </nav>
        </div>
      </div><div></div>
    </div>
    
  </header>
