<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "FrontController@index");
Route::get('/MTI-act-rules-regulations', "FrontController@MTIActRulesRegulations");
Route::get('/careers', "FrontController@careers");
Route::get('/services', "FrontController@services");
Route::get('/tenders', "FrontController@tenders");
Route::get('/get-tender-details/{id?}', "FrontController@getTenderDetails");
Route::get('/downloads', "FrontController@donwloads");
Route::get('/contact-us', "FrontController@contactUs");
Route::get('/coming-soon', "FrontController@comingSoon");
Route::get('/news-details/{id}', "FrontController@showNewsDetails");
Route::get('/doctor-details/{id}', "FrontController@showDoctorDetails");
Route::get('/about-us', "FrontController@showAboutUs");
Route::get('/vision-mission', "FrontController@showVisionMission");
Route::get('/governors-board', "FrontController@showBoardofGovernors");
Route::get('/timeline', "FrontController@showTimeline");

