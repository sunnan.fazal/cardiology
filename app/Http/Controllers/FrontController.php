<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\BOG;
use App\Careers;
use App\Doctors;
use App\Downloads;
use App\FrontContentSection;
use App\MTI_Acts;
use App\News;
use App\Slider;
use App\Tenders;
use App\Timeline;

class FrontController extends Controller
{
    
	public function index()
	{
	    $first_content = FrontContentSection::where("content_level",1)->get();
	    $second_content = FrontContentSection::where("content_level",2)->get();
	    $third_content = FrontContentSection::where("content_level",3)->get();
		$news = News::where('active' , 1)->orderByDesc('updated_at')->get();
		$sliders = Slider::all();
		$doctors = Doctors::all();
		return view("main.index" , compact('news','first_content','second_content','third_content','sliders' , 'doctors'));
	}

	public function MTIActRulesRegulations()
	{
		$mti_act = MTI_Acts::where('active' , 1)->get();
		return view("main.mti-act" , compact('mti_act'));
	}

	public function careers()
	{
		$careers = Careers::where("advertisement_date" , "<=" , date('Y-m-d'))->where('closing_date' , '>=' , date('Y-m-d'))->where('active' , 1)->orderByDesc("advertisement_date")->get();
		return view("main.careers" , compact('careers'));
	}
	public function tenders()
	{
		$tenders = Tenders::where("advertisement_date" , "<=" , date('Y-m-d'))->where('closing_date' , '>=' , date('Y-m-d'))->where('active' , 1)->orderByDesc("advertisement_date")->get();
		return view("main.tenders" , compact('tenders'));
	}

	public function getTenderDetails($id)
	{
		echo strip_tags(htmlspecialchars_decode(Tenders::where('id',$id)->first()->tender_description));
	}

	public function showNewsDetails($id)
	{
		$news = News::where('id' , $id)->first();
		return view("main.news-details" , compact('news'));	
	}

	public function services()
	{
		return view("main.services");
	}
	public function donwloads()
	{
		$downloads = Downloads::all();
		return view("main.downloads" , compact('downloads'));
	}
	public function contactUs()
	{
		return view("main.contact-us");
	}
	public function comingSoon()
	{
		return view("coming-soon");
	}

	public function showDoctorDetails($doctorId)
	{
		$doctor = Doctors::where("id" , $doctorId)->get();
		return view("main.doctor-details" , compact('doctor'));
	}

	public function showAboutUs()
	{
		$about = AboutUs::where('type','about-us')->get();
		return view("main.about-us" , compact('about'));
	}

	public function showVisionMission()
	{
		$about = AboutUs::where('type','vision-mission')->get();
		return view("main.vision-mission" , compact('about'));
	}

	public function showBoardofGovernors()
	{
		$bog = BOG::all();
		return view("main.board-governors" , compact('bog'));
	}

	public function showTimeline()
	{
		$timeline = Timeline::all();
		return view("main.timeline" , compact('timeline'));
	}

}


?>
